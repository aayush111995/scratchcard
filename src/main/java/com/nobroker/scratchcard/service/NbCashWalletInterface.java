package com.nobroker.scratchcard.service;

import com.nobroker.scratchcard.model.ClaimCreationForm;
import com.nobroker.scratchcard.model.NbCashTopUpRespose;
import com.nobroker.scratchcard.model.ResponseDTO;
import com.nobroker.scratchcard.model.WalletCashTopupForm;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Aayush Chaudhary
 */
public interface NbCashWalletInterface {

    @Headers("Content-Type: application/json")
    @POST("v2/wallet/topup")
    Call<ResponseDTO<NbCashTopUpRespose>> topUp(@Header("x-api-key") String apiKey, @Body WalletCashTopupForm payload);

    @Headers("Content-Type: application/json")
    @POST("v2/claim/per/user/cash")
    Call<ResponseDTO<Object>> createClaimAndCreditNbCash(@Header("x-api-key") String apiKey, @Body ClaimCreationForm payload);
}
