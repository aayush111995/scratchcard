package com.nobroker.scratchcard.service;

import com.nobroker.scratchcard.constants.ScratchCardConstants;
import com.nobroker.scratchcard.entity.CashBack;
import com.nobroker.scratchcard.entity.ContestDetail;
import com.nobroker.scratchcard.entity.ScratchCard;
import com.nobroker.scratchcard.entity.UserDetail;
import com.nobroker.scratchcard.enums.CashBackState;
import com.nobroker.scratchcard.enums.CashTransactionType;
import com.nobroker.scratchcard.enums.OfferType;
import com.nobroker.scratchcard.model.*;
import com.nobroker.scratchcard.redis.RedisService;
import com.nobroker.scratchcard.utils.JSONUtil;
import com.nobroker.scratchcard.utils.RetrofitUtil;
import com.nobroker.scratchcard.utils.ScratchCardUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;

/**
 * Aayush Chaudhary
 */
@Service
public class CashBackOptionProcess implements OptionProcess<ProcessOptionModel>, PostScratchProcess<ScratchCardInfo> {
    public static Logger logger = LoggerFactory.getLogger(CashBackOptionProcess.class);

    @Autowired
    private RedisService redisService;
    @Autowired
    private CashBackService cashBackService;
    @Autowired
    private ScratchCardService scratchCardService;

    @Value("${spring.scratch-card.cash-back.expiry-in-months:1}")
    private int cashBackExpiry;

    private RealCashInterface realCashInterface;


    public CashBackOptionProcess(@Value("${spring.scratch-card.cash-back.base-url:https://www.nobroker.in/api/}") String baseUrl) {
        Retrofit retrofit = RetrofitUtil.getRetrofit(baseUrl, true);
        realCashInterface = retrofit.create(RealCashInterface.class);
    }

    @Override
    @Transactional
    public boolean processOption(ProcessOptionModel processOptionModel) {
        ScratchCardPayload scratchCardPayload = processOptionModel.getScratchCardPayload();
        ContestDetail contestDetail = processOptionModel.getContestDetail();
        BigDecimal minCashValue = contestDetail.getMinCashValue();
        BigDecimal maxCashValue = contestDetail.getMaxCashValue();
        OptionJson optionJson = processOptionModel.getOptionJson();
        String dailyCashBackLimit = optionJson.getDailyCashBackLimit();
        Optional<String> dailyCashBackCount = redisService.getValueFromCache(ScratchCardConstants.DAILYCASHBACKCOUNT.concat(contestDetail.getId()));
        if (dailyCashBackCount.isPresent() && dailyCashBackLimit != null) {
            if (Integer.parseInt(dailyCashBackLimit) <= Integer.parseInt(dailyCashBackCount.get())) {
                logger.info("daily cashback limit limit exceeds for the contest {} reference id {} ", contestDetail.getId(), processOptionModel.getScratchCardPayload().getReferenceId());
                return false;
            }
        }
        int cashBackAmount = ScratchCardUtils.getRandomValueInRange(minCashValue.intValue(), maxCashValue.intValue());
        CashBack cashBack = saveCashBack(cashBackAmount, contestDetail);
        scratchCardPayload.setOfferId(cashBack.getId());
        scratchCardPayload.setOfferType(OfferType.CASHBACK);
        scratchCardPayload.setExpiryDate(ScratchCardUtils.addMonths(new Date(), cashBackExpiry));
        scratchCardPayload.setScratchCardState(processOptionModel.getScratchCardState());
        scratchCardPayload.setContestDetail(contestDetail);
        scratchCardService.saveScratchCard(scratchCardPayload);
        if (dailyCashBackCount.isPresent()) {
            redisService.setKeyValueWithExpiry(ScratchCardConstants.DAILYCASHBACKCOUNT.concat(contestDetail.getId()), String.valueOf(Integer.parseInt(dailyCashBackCount.get()) + 1), 1);
        } else {
            redisService.setKeyValueWithExpiry(ScratchCardConstants.DAILYCASHBACKCOUNT.concat(contestDetail.getId()), String.valueOf(1), 1);

        }
        return true;
    }

    private CashBack saveCashBack(int cashBackAmount, ContestDetail contestDetail) {
        CashBack cashBack = new CashBack();
        cashBack.setAmount(new BigDecimal(cashBackAmount));
        cashBack.setCashBackState(CashBackState.INITIATED);
        cashBack.setLastUpdatedDate(new Date());
        cashBack.setCreatedDate(new Date());
        cashBack.setCashTransactionType(CashTransactionType.CASH_BACK);
        cashBack.setContestDetail(contestDetail);
        cashBackService.save(cashBack);
        return cashBack;
    }

    @Override
    public void postProcessScratchCard(ScratchCardInfo scratchCardInfo) {
        CashBack cashBack = scratchCardInfo.getCashBack();
        ScratchCard scratchCard = scratchCardInfo.getScratchCard();
        UserDetail userDetail = scratchCard.getUserDetail();
        logger.info("post process real cash user id {} ref id {}", userDetail.getUserId(), scratchCard.getReferenceId());
        RealCashTopup realCashTopup = RealCashTopup.builder().refId(cashBack.getId())
                .amount(cashBack.getAmount())
                .type(ScratchCardConstants.REAL_TOP_UP_TYPE).build();
//        scratchCard.setScratchCardState(ScratchCardState.SCRATCHED);
//        scratchCard.setLastUpdatedDate(new Date());
//        scratchCardService.save(scratchCard);
        Call<ResponseDTO<NbCashTopUpRespose>> responseEntityCall = realCashInterface.creditCashBack(realCashTopup.getRefId(), realCashTopup.getType(), realCashTopup.getAmount());
        try {
            Response<ResponseDTO<NbCashTopUpRespose>> responseEntityResponse = responseEntityCall.execute();
            ResponseDTO<NbCashTopUpRespose> body = responseEntityResponse.body();
            if (responseEntityResponse.code() == 200) {
                logger.info("reference id {} user id {} body of response {}", scratchCard.getReferenceId(), userDetail.getUserId(), JSONUtil.getStringFromObject(body));
                if (body != null) {
                    NbCashTopUpRespose data = body.getData();
                    cashBack.setTransactionId(data.getId());
                    cashBack.setCashBackState(CashBackState.PROCESSING);
                    cashBack.setLastUpdatedDate(new Date());
                    cashBackService.save(cashBack);
                }
            } else {
                logger.error("reference id {} user id {} body top up error response code  {}", scratchCard.getReferenceId(), userDetail.getUserId(), responseEntityResponse.code());

            }

        } catch (Exception e) {
            logger.error("exception in calling cash back api {} ref id {} user id {} ", e.getMessage(), scratchCard.getReferenceId(), userDetail.getUserId());
        }
    }
}
