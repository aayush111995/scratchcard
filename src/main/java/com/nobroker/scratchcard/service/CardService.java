package com.nobroker.scratchcard.service;

import com.nobroker.scratchcard.constants.ScratchCardConstants;
import com.nobroker.scratchcard.entity.CashBack;
import com.nobroker.scratchcard.entity.ContestDetail;
import com.nobroker.scratchcard.entity.CouponCode;
import com.nobroker.scratchcard.entity.ScratchCard;
import com.nobroker.scratchcard.enums.OfferType;
import com.nobroker.scratchcard.enums.ScratchCardState;
import com.nobroker.scratchcard.kafka.KafkaProducer;
import com.nobroker.scratchcard.model.AssignCoupon;
import com.nobroker.scratchcard.model.OptionJson;
import com.nobroker.scratchcard.model.ScratchCardInfo;
import com.nobroker.scratchcard.redis.RedisService;
import com.nobroker.scratchcard.utils.JSONUtil;
import com.nobroker.scratchcard.utils.ScratchCardUtils;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Aayush Chaudhary
 */
@Service
@AllArgsConstructor
public class CardService {
    public static Logger logger = LoggerFactory.getLogger(CardService.class);

    private final ScratchCardService scratchCardService;
    private final CouponCodeService couponCodeService;
    private final CashBackService cashBackService;
    private final KafkaProducer kafkaProducer;
    private final RedisService redisService;

    public ScratchCardInfo getScratchCardInfoAndPostProcess(String scratchCardId) {
        ScratchCardInfo scratchCardInfo = new ScratchCardInfo();
        Optional<ScratchCard> optionalScratchCard = scratchCardService.findScratchCardById(scratchCardId);
        if (optionalScratchCard.isPresent()) {
            ScratchCard scratchCard = optionalScratchCard.get();
                OfferType offerType = scratchCard.getOfferType();
            if (scratchCard.getOfferId() != null) {
                switch (offerType) {
                    case NB_CASH:
                         Optional<CashBack> optionalNbCashBack = cashBackService.findById(scratchCard.getOfferId());
                        scratchCardInfo.setCashBack(optionalNbCashBack.orElse(null));
                        scratchCardInfo.setUiJson(ScratchCardUtils.getFrontEndJsonForNbCash());
                        break;
                    case CASHBACK:
                        Optional<CashBack> optionalCashBack = cashBackService.findById(scratchCard.getOfferId());
                        scratchCardInfo.setCashBack(optionalCashBack.orElse(null));
                        scratchCardInfo.setUiJson(ScratchCardUtils.getFrontEndJsonForCashBack());
                        break;
                    case BETTER_LUCK:
                        scratchCardInfo.setUiJson(ScratchCardUtils.getFrontEndJsonForBetterLuck());
                        break;

                }
            }
            scratchCardInfo.setScratchCard(scratchCard);
            if (ScratchCardState.UNLOCKED == scratchCard.getScratchCardState()) {
                scratchCard.setScratchCardState(ScratchCardState.SCRATCHED);
                scratchCard.setLastUpdatedDate(new Date());
                scratchCardService.save(scratchCard);
                kafkaProducer.producePostScratchCardEvent(scratchCardInfo);
            }
        }
        return scratchCardInfo;
    }

    public List<ScratchCardInfo> getAllScratchCardOfUser(String userId) {
        List<ScratchCard> scratchCardList = scratchCardService.findScratchCardsByUserId(userId);
        List<ScratchCardInfo> scratchCardInfos = new ArrayList<>();
        for (ScratchCard scratchCard : scratchCardList) {
            ScratchCardInfo scratchCardInfo = new ScratchCardInfo();
            scratchCardInfo.setScratchCard(scratchCard);
            OfferType offerType = scratchCard.getOfferType();
            if (scratchCard.getOfferId() != null) {
                switch (offerType) {
                    case NB_CASH:
                        Optional<CashBack> optionalNbCashBack = cashBackService.findById(scratchCard.getOfferId());
                        scratchCardInfo.setCashBack(optionalNbCashBack.orElse(null));
                        scratchCardInfo.setUiJson(ScratchCardUtils.getFrontEndJsonForNbCash());
                        break;
                    case CASHBACK:
                        Optional<CashBack> optionalCashBack = cashBackService.findById(scratchCard.getOfferId());
                        scratchCardInfo.setCashBack(optionalCashBack.orElse(null));
                        scratchCardInfo.setUiJson(ScratchCardUtils.getFrontEndJsonForCashBack());
                        break;
                    case COUPON:
                        Optional<CouponCode> optionalCouponCode = couponCodeService.findById(scratchCard.getOfferId());
                        scratchCardInfo.setCouponCode(optionalCouponCode.orElse(null));
                        break;
                    case BETTER_LUCK:
                        scratchCardInfo.setUiJson(ScratchCardUtils.getFrontEndJsonForBetterLuck());
                        break;
                }
            }
            scratchCardInfos.add(scratchCardInfo);
        }
        return scratchCardInfos;
    }

    public ScratchCardInfo assignCouponCode(AssignCoupon assignCoupon) {
        ScratchCardInfo scratchCardInfo = new ScratchCardInfo();
        Optional<ScratchCard> optionalScratchCard = scratchCardService.findScratchCardById(assignCoupon.getScratchCardId());
        if (optionalScratchCard.isPresent()) {
            ScratchCard scratchCard = optionalScratchCard.get();
            scratchCardInfo.setScratchCard(scratchCard);
            if (ScratchCardState.UNLOCKED == scratchCard.getScratchCardState()) {
                ContestDetail contestDetail = scratchCard.getContestDetail();
                OptionJson optionJson = JSONUtil.getObjectFromJsonString(contestDetail.getOptionsJson(), OptionJson.class);
                if (optionJson != null) {
                    String dailyContestCouponLimit = optionJson.getDailyCouponLimit();
                    Optional<String> dailyCouponCount = redisService.getValueFromCache(ScratchCardConstants.DAILYCOUPONCOUNT.concat(contestDetail.getId()));
                    if (dailyCouponCount.isPresent() && dailyContestCouponLimit != null) {
                        if (Integer.parseInt(dailyContestCouponLimit) <= Integer.parseInt(dailyCouponCount.get())) {
                            logger.info("daily coupon limit exceeds for the contest {} reference id {} ", contestDetail.getId(), scratchCard.getReferenceId());
                            scratchCard.setOfferId(String.valueOf(new Date().getTime()));
                            scratchCard.setScratchCardState(ScratchCardState.ASSIGNED);
                            scratchCard.setOfferType(OfferType.BETTER_LUCK);
                            scratchCard.setLastUpdatedDate(new Date());
                            scratchCardService.save(scratchCard);
                            scratchCardInfo.setScratchCard(scratchCard);
                            return scratchCardInfo;
                        }
                    }
                    OfferType offerType = scratchCard.getOfferType();
                    if (OfferType.COUPON == offerType && scratchCard.getOfferId() == null) {
                        List<String> couponDetailIds = assignCoupon.getCouponDetailIds();
                        if (couponDetailIds != null) {

                            List<CouponCode> couponCodes = couponCodeService.getByClientIdAndCouponDetailId(assignCoupon.getClientId(), assignCoupon.getCouponDetailIds(), PageRequest.of(0, ScratchCardConstants.MaxCouponResults));

                            if (couponCodes != null && !couponCodes.isEmpty()) {
                                int randomValue = ScratchCardUtils.getRandomValueInRange(1, couponCodes.size());
                                CouponCode couponCode = couponCodes.get(randomValue - 1);
                                scratchCard.setOfferId(couponCode.getId());
                                scratchCard.setExpiryDate(couponCode.getExpiryDate());
                                scratchCard.setScratchCardState(ScratchCardState.ASSIGNED);
                                scratchCard.setLastUpdatedDate(new Date());
                                scratchCardService.save(scratchCard);
                                logger.info("making coupon inactive coupon id {} for reference id {}", couponCode.getId(), scratchCard.getReferenceId());
                                couponCode.setLastUpdatedDate(new Date());
                                couponCode.setIsActive(false);
                                couponCodeService.saveCouponCode(couponCode);
                                scratchCardInfo.setCouponCode(couponCode);
                                if (dailyCouponCount.isPresent()) {
                                    redisService.setKeyValueWithExpiry(ScratchCardConstants.DAILYCOUPONCOUNT.concat(contestDetail.getId()), String.valueOf(Integer.parseInt(dailyCouponCount.get()) + 1), 1);
                                } else {
                                    redisService.setKeyValueWithExpiry(ScratchCardConstants.DAILYCOUPONCOUNT.concat(contestDetail.getId()), String.valueOf(1), 1);

                                }
                            } else {
                                logger.info("coupons are not available so better luck {} id ", scratchCard.getId());
                                scratchCard.setOfferId(String.valueOf(new Date().getTime()));
                                scratchCard.setScratchCardState(ScratchCardState.ASSIGNED);
                                scratchCard.setOfferType(OfferType.BETTER_LUCK);
                                scratchCard.setExpiryDate(ScratchCardUtils.addMonths(new Date(), 1));
                                scratchCard.setLastUpdatedDate(new Date());
                                scratchCardService.save(scratchCard);
                            }

                        }
                    } else {
                        logger.info("offer type is not coupon or offer id is not null id {} ", scratchCard.getId());

                    }
                } else {
                    logger.info("option json is null for the contest {} ref id {} scratch id {}", contestDetail.getId(), scratchCard.getReferenceId(), assignCoupon.getScratchCardId());
                }
            } else {
                logger.info("state of scratch card is not in unlock state the id {}", assignCoupon.getScratchCardId());
                if (OfferType.COUPON == scratchCard.getOfferType() && scratchCard.getOfferId() != null) {
                    Optional<CouponCode> optionalCouponCode = couponCodeService.findById(scratchCard.getOfferId());
                    scratchCardInfo.setCouponCode(optionalCouponCode.orElse(null));
                }
            }
            scratchCardInfo.setScratchCard(scratchCard);
        } else {
            logger.info("scratch card not found for the id {}", assignCoupon.getScratchCardId());

        }

        return scratchCardInfo;
    }


}
