package com.nobroker.scratchcard.service;

import com.nobroker.scratchcard.constants.ScratchCardConstants;
import com.nobroker.scratchcard.enums.Options;
import com.nobroker.scratchcard.enums.ScratchCardRandomStrategyName;
import com.nobroker.scratchcard.model.OptionJson;
import com.nobroker.scratchcard.redis.RedisService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Aayush Chaudhary
 */
@Service
public class ModulusPickStrategy implements ScratchCardRandomStrategy {
    public static Logger logger = LoggerFactory.getLogger(ModulusPickStrategy.class);

    @Autowired
    private RedisService redisService;


    @Override
    public Options pickRandomOption(OptionJson optionJson) {
        Optional<String> dailyUserCount = redisService.getValueFromCache(ScratchCardConstants.DAILYUSERCOUNT);
        AtomicReference<Options> option = new AtomicReference<>(Options.BETTERLUCK);
        if (dailyUserCount.isPresent()) {
            Integer count = Integer.parseInt(dailyUserCount.get());
            Map<Options, Integer> map = new TreeMap<>();
            map.put(Options.COUPON, Integer.parseInt(optionJson.getCoupon()));
            map.put(Options.CASHBACK, Integer.parseInt(optionJson.getCashBack()));
            map.put(Options.NBCASH, Integer.parseInt(optionJson.getNbCash()));
            map.put(Options.BETTERLUCK, Integer.parseInt(optionJson.getBetterLuckNextTime()));
            Optional<Map.Entry<Options, Integer>> optionalEntry = map.entrySet()
                    .stream()
                    .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                    .filter(entry -> count % entry.getValue() == 0)
                    .findFirst();
            optionalEntry.ifPresent(entry -> option.set(entry.getKey()));

            logger.info("count " + count);

        }
        logger.info("option " + option.get());
        return option.get();
    }

    @Override
    public ScratchCardRandomStrategyName getScratchCardRandomStrategyName() {
        return ScratchCardRandomStrategyName.MODULUS_PICK;
    }
}
