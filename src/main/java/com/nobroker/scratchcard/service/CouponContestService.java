package com.nobroker.scratchcard.service;

import com.nobroker.scratchcard.dao.CouponContestRepository;
import com.nobroker.scratchcard.entity.CouponContest;
import com.nobroker.scratchcard.model.CouponContestModel;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Aayush Chaudhary
 */
@Service
@AllArgsConstructor
public class CouponContestService {
    public static Logger logger = LoggerFactory.getLogger(CouponContestService.class);

    private CouponContestRepository couponContestRepository;


    public Optional<CouponContest> findById(String id) {
        return couponContestRepository.findById(id);
    }

    public List<CouponContest> findAll() {
        return couponContestRepository.findAll();
    }

    public List<CouponContest> findByContestId(String contestId) {
        return couponContestRepository.findByContestId(contestId);
    }


    @Transactional
    public void saveCouponContest(CouponContest couponContest) {
        couponContestRepository.save(couponContest);
    }

    @Transactional
    public int loadCouponContestMap(List<CouponContestModel> couponContestModels) {
        List<CouponContest> couponContests = new ArrayList<>();
        for (CouponContestModel contestModel : couponContestModels) {
            try {
                CouponContest couponContest = new CouponContest();
                couponContest.setContestId(contestModel.getContestId());
                couponContest.setCouponDetailId(contestModel.getCouponDetailId());
                saveCouponContest(couponContest);
                couponContests.add(couponContest);
            } catch (Exception exception) {
                logger.error("exception in loading the coupon contest in   contest id {} coupon detail id {}  exception {}", contestModel.getContestId(), contestModel.getCouponDetailId(), exception);
            }
        }
        return couponContests.size();
    }
}
