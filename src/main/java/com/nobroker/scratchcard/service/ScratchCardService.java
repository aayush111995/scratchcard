package com.nobroker.scratchcard.service;

import com.nobroker.scratchcard.dao.ScratchCardRepository;
import com.nobroker.scratchcard.entity.ScratchCard;
import com.nobroker.scratchcard.model.ScratchCardPayload;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Aayush Chaudhary
 */
@Service
@AllArgsConstructor
public class ScratchCardService {

    private final ScratchCardRepository scratchCardRepository;

    public void save(ScratchCard scratchCard) {
        scratchCardRepository.save(scratchCard);
    }

    public void saveScratchCard(ScratchCardPayload scratchCardPayload) {
        ScratchCard scratchCard = new ScratchCard();
        scratchCard.setUserDetail(scratchCardPayload.getUserDetail());
        scratchCard.setOfferId(scratchCardPayload.getOfferId());
        scratchCard.setOfferType(scratchCardPayload.getOfferType());
        scratchCard.setCreatedDate(new Date());
        scratchCard.setLastUpdatedDate(new Date());
        scratchCard.setReferenceId(scratchCardPayload.getReferenceId());
        scratchCard.setOfferReason(scratchCardPayload.getFlowName());
        scratchCard.setScratchCardState(scratchCardPayload.getScratchCardState());
        scratchCard.setContestDetail(scratchCardPayload.getContestDetail());
        scratchCard.setExpiryDate(scratchCardPayload.getExpiryDate());
        save(scratchCard);
    }

    public Optional<ScratchCard> findScratchCardByRefId(String refId) {
        return scratchCardRepository.findByReferenceId(refId);
    }

    public List<ScratchCard> findScratchCardsByUserId(String userId) {
        return scratchCardRepository.findByUserId(userId);
    }


    public Optional<ScratchCard> findScratchCardById(String id) {
        return scratchCardRepository.findById(id);
    }

}
