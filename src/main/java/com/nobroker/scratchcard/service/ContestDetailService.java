package com.nobroker.scratchcard.service;

import com.nobroker.scratchcard.dao.ContestDetailRepository;
import com.nobroker.scratchcard.entity.ContestDetail;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Aayush Chaudhary
 */
@Service
@AllArgsConstructor
public class ContestDetailService {

    private final ContestDetailRepository contestDetailRepository;

    public Optional<ContestDetail> getContestById(String id) {
        return contestDetailRepository.findById(id);
    }

    public List<ContestDetail> getContestsByFlowName(String flowName) {
        return contestDetailRepository.findByFlowName(flowName);
    }

}
