package com.nobroker.scratchcard.service;

import com.nobroker.scratchcard.enums.Options;
import com.nobroker.scratchcard.enums.ScratchCardRandomStrategyName;
import com.nobroker.scratchcard.model.OptionJson;
import org.springframework.stereotype.Service;

/**
 * Aayush Chaudhary
 */
@Service
public class RandomPickStrategy implements ScratchCardRandomStrategy {
    @Override
    public Options pickRandomOption(OptionJson optionJson) {
        return null;
    }

    @Override
    public ScratchCardRandomStrategyName getScratchCardRandomStrategyName() {
        return ScratchCardRandomStrategyName.RANDOM_PICK;
    }
}
