package com.nobroker.scratchcard.service;

import com.nobroker.scratchcard.enums.OfferType;
import com.nobroker.scratchcard.model.ProcessOptionModel;
import com.nobroker.scratchcard.model.ScratchCardInfo;
import com.nobroker.scratchcard.model.ScratchCardPayload;
import com.nobroker.scratchcard.redis.RedisService;
import com.nobroker.scratchcard.utils.ScratchCardUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Aayush Chaudhary
 */
@Service
public class BetterLuckOptionProcess implements OptionProcess<ProcessOptionModel>, PostScratchProcess<ScratchCardInfo> {

    @Autowired
    private RedisService redisService;

    @Autowired
    private ScratchCardService scratchCardService;

    @Override
    public boolean processOption(ProcessOptionModel processOptionModel) {
        ScratchCardPayload scratchCardPayload = processOptionModel.getScratchCardPayload();
        scratchCardPayload.setOfferId(String.valueOf(new Date().getTime()));
        scratchCardPayload.setExpiryDate(ScratchCardUtils.addMonths(new Date(), 1));
        scratchCardPayload.setOfferType(OfferType.BETTER_LUCK);
        scratchCardPayload.setScratchCardState(processOptionModel.getScratchCardState());
        scratchCardPayload.setContestDetail(processOptionModel.getContestDetail());
        scratchCardService.saveScratchCard(scratchCardPayload);
        return true;
    }

    @Override
    public void postProcessScratchCard(ScratchCardInfo scratchCardInfo) {
//        ScratchCard scratchCard = scratchCardInfo.getScratchCard();
//        scratchCard.setScratchCardState(ScratchCardState.SCRATCHED);
//        scratchCard.setLastUpdatedDate(new Date());
//        scratchCardService.save(scratchCard);
    }
}
