package com.nobroker.scratchcard.service;

import com.nobroker.scratchcard.entity.ContestDetail;
import com.nobroker.scratchcard.entity.CouponCode;
import com.nobroker.scratchcard.entity.ScratchCard;
import com.nobroker.scratchcard.entity.UserDetail;
import com.nobroker.scratchcard.enums.CouponCompany;
import com.nobroker.scratchcard.enums.OfferType;
import com.nobroker.scratchcard.enums.ScratchCardState;
import com.nobroker.scratchcard.model.*;
import com.nobroker.scratchcard.redis.RedisService;
import com.nobroker.scratchcard.utils.JSONUtil;
import com.nobroker.scratchcard.utils.RetrofitUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

import java.util.Date;

/**
 * Aayush Chaudhary
 */
@Service
public class CouponOptionProcess implements OptionProcess<ProcessOptionModel>, PostScratchProcess<ScratchCardInfo> {
    public static Logger logger = LoggerFactory.getLogger(CouponOptionProcess.class);

    @Autowired
    private RedisService redisService;
    @Autowired
    private CouponCodeService couponCodeService;
    @Autowired
    private ScratchCardService scratchCardService;
    private NbCashWalletInterface nbCashWalletInterface;
    @Value("${spring.scratch-card.nb-cash.wallet-api-key-value:nb-w4113t-1qaz}")
    private String walletApiKeyValue;


    public CouponOptionProcess(@Value("${spring.scratch-card.nb-cash.base-url:https://www.nobroker.in/wallet-api/}") String baseUrl) {
        Retrofit retrofit = RetrofitUtil.getRetrofit(baseUrl, true);
        nbCashWalletInterface = retrofit.create(NbCashWalletInterface.class);
    }

    @Override
    public boolean processOption(ProcessOptionModel processOptionModel) {
        ContestDetail contestDetail = processOptionModel.getContestDetail();
        ScratchCardPayload scratchCardPayload = processOptionModel.getScratchCardPayload();
        scratchCardPayload.setOfferType(OfferType.COUPON);
        scratchCardPayload.setScratchCardState(processOptionModel.getScratchCardState());
        scratchCardPayload.setContestDetail(contestDetail);
        scratchCardService.saveScratchCard(scratchCardPayload);
        return true;
    }


    @Override
    public void postProcessScratchCard(ScratchCardInfo scratchCardInfo) {

        CouponCode couponCode = scratchCardInfo.getCouponCode();
        ScratchCard scratchCard = scratchCardInfo.getScratchCard();
        scratchCard.setScratchCardState(ScratchCardState.SCRATCHED);
        scratchCard.setLastUpdatedDate(new Date());
        scratchCardService.save(scratchCard);
        if (couponCode != null && couponCode.getCouponProvider() == CouponCompany.NOBROKER) {
            String couponCodeId = couponCode.getId();
            UserDetail userDetail = scratchCard.getUserDetail();
            if (userDetail != null) {
                ClaimCreationForm claimCreationForm = ClaimCreationForm.builder()
                        .claimId(couponCodeId)
                        .email(userDetail.getEmail())
                        .userId(userDetail.getUserId())
                        .mobileNumber(userDetail.getMobileNumber())
                        .referenceId(scratchCard.getReferenceId())
                        .build();
                Call<ResponseDTO<Object>> responseEntityCall = nbCashWalletInterface.createClaimAndCreditNbCash(walletApiKeyValue, claimCreationForm);
                try {
                    Response<ResponseDTO<Object>> responseEntityResponse = responseEntityCall.execute();
                    logger.info("response from the claim creation and nb cash {} ref id {} coupon id {}", JSONUtil.getStringFromObject(responseEntityResponse), scratchCard.getReferenceId(), couponCodeId);
                } catch (Exception exception) {
                    logger.error("exception in executing the claim creation and credit nb cash {} ref id {} coupon id {}", exception, scratchCard.getReferenceId(), couponCode.getId());
                }

            }
        } else {
            logger.info("coupon provider is not nobroker so doing nothing ref id {}", scratchCard.getReferenceId());
        }
    }
}
