package com.nobroker.scratchcard.service;

import com.nobroker.scratchcard.model.NbCashTopUpRespose;
import com.nobroker.scratchcard.model.ResponseDTO;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

import java.math.BigDecimal;

/**
 * Aayush Chaudhary
 */
public interface RealCashInterface {

    @FormUrlEncoded
    @POST("v1/cashback/add")
    Call<ResponseDTO<NbCashTopUpRespose>> creditCashBack(@Field("refId") String refId, @Field("type") String type, @Field("amount") BigDecimal amount);

}
