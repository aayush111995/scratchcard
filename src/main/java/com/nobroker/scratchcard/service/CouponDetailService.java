package com.nobroker.scratchcard.service;

import com.nobroker.scratchcard.dao.CouponDetailRepository;
import com.nobroker.scratchcard.entity.CouponContest;
import com.nobroker.scratchcard.entity.CouponDetail;
import com.nobroker.scratchcard.model.CouponDetailModel;
import com.nobroker.scratchcard.utils.JSONUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Aayush Chaudhary
 */
@Service
@AllArgsConstructor
public class CouponDetailService {

    private final CouponDetailRepository couponDetailRepository;

    public List<CouponDetail> getAllCouponDetails() {
        return couponDetailRepository.findAll();
    }

    public List<CouponDetail> getAllCouponDetailsWithCashBackAndNbCash() {
        //        CouponDetail cashBack = new CouponDetail();
//        cashBack.setId(ScratchCardConstants.CASHBACK);
//        cashBack.setCouponJson(ScratchCardUtils.getFrontEndJsonForCashBack());
//        allCouponDetails.add(cashBack);
//        CouponDetail nbCash = new CouponDetail();
//        nbCash.setId(ScratchCardConstants.NBCASH);
//        nbCash.setCouponJson(ScratchCardUtils.getFrontEndJsonForNbCash());
//        allCouponDetails.add(nbCash);
        return getAllCouponDetails();
    }

    public Optional<CouponDetail> findById(String id) {
        return couponDetailRepository.findById(id);
    }

    @Transactional
    public void save(CouponDetail couponDetail) {
        couponDetailRepository.save(couponDetail);
    }

    public int loadCouponDetails(List<CouponDetailModel> couponDetailModelList) {
        List<CouponDetail> couponDetails = new ArrayList<>();
        for (CouponDetailModel couponDetailModel : couponDetailModelList) {
            CouponDetail couponDetail = new CouponDetail();
            couponDetail.setId(couponDetailModel.getId());
            couponDetail.setCouponJson(JSONUtil.getStringFromObject(couponDetailModel.getCouponDetailJson()));
            save(couponDetail);
            couponDetails.add(couponDetail);
        }
        return couponDetails.size();
    }

    public List<CouponDetail> findAllCouponDetailByIds(List<String> ids) {
        return couponDetailRepository.findByIdIn(ids);
    }

    public List<CouponDetail> getCouponDetailsFromCouponContest(List<CouponContest> couponContests) {
        List<String> ids = new ArrayList<>();
        for (CouponContest couponContest : couponContests) {
            ids.add(couponContest.getCouponDetailId());
        }
        return findAllCouponDetailByIds(ids);
    }


}
