package com.nobroker.scratchcard.service;

import com.nobroker.scratchcard.dao.UserDetailRepository;
import com.nobroker.scratchcard.entity.UserDetail;
import com.nobroker.scratchcard.model.ScratchCardEventPayload;
import com.nobroker.scratchcard.model.UserInfo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.Optional;

/**
 * Aayush Chaudhary
 */
@Service
@AllArgsConstructor
public class UserDetailService {

    private final UserDetailRepository userDetailRepository;

    @Transactional
    public void saveUserDetail(UserDetail userDetail) {
        userDetailRepository.save(userDetail);
    }

    public Optional<UserDetail> findUserDetailById(String userId) {
        return userDetailRepository.findById(userId);
    }

    public UserDetail saveUserInfoIfNotExistsEvent(ScratchCardEventPayload scratchCardEventPayload) {
        UserInfo userInfo = scratchCardEventPayload.getUserInfo();
        Optional<UserDetail> optionalUserDetail = findUserDetailById(userInfo.getUserId());
        if (optionalUserDetail.isPresent()) {
            return optionalUserDetail.get();
        }
        UserDetail userDetail = new UserDetail();
        userDetail.setUserId(userInfo.getUserId());
        userDetail.setCreatedDate(new Date());
        userDetail.setEmail(userInfo.getEmail());
        userDetail.setName(userInfo.getUserName());
        userDetail.setUserSource(userInfo.getUserSource());
        userDetail.setMobileNumber(userInfo.getMobileNumber());
        saveUserDetail(userDetail);
        return userDetail;
    }
}
