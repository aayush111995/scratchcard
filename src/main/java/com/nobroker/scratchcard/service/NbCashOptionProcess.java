package com.nobroker.scratchcard.service;

import com.nobroker.scratchcard.constants.ScratchCardConstants;
import com.nobroker.scratchcard.entity.CashBack;
import com.nobroker.scratchcard.entity.ContestDetail;
import com.nobroker.scratchcard.entity.ScratchCard;
import com.nobroker.scratchcard.entity.UserDetail;
import com.nobroker.scratchcard.enums.CashBackState;
import com.nobroker.scratchcard.enums.CashTransactionType;
import com.nobroker.scratchcard.enums.OfferType;
import com.nobroker.scratchcard.model.*;
import com.nobroker.scratchcard.redis.RedisService;
import com.nobroker.scratchcard.utils.JSONUtil;
import com.nobroker.scratchcard.utils.RetrofitUtil;
import com.nobroker.scratchcard.utils.ScratchCardUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Optional;

/**
 * Aayush Chaudhary
 */
@Service
public class NbCashOptionProcess implements OptionProcess<ProcessOptionModel>, PostScratchProcess<ScratchCardInfo> {
    public static Logger logger = LoggerFactory.getLogger(NbCashOptionProcess.class);

    @Autowired
    private RedisService redisService;
    @Autowired
    private CashBackService cashBackService;
    @Autowired
    private ScratchCardService scratchCardService;
    @Value("${spring.scratch-card.nb-cash.expiry-in-months:1}")
    private int nbCashExpiry;

    @Value("${spring.scratch-card.nb-cash.expiry-nb-cash-in-months:1}")
    private int nbCashWalletExpiry;

    @Value("${spring.scratch-card.nb-cash.wallet-api-key-value:nb-w4113t-1qaz}")
    private String walletApiKeyValue;

    @Value("${spring.scratch-card.nb-cash.wallet-claim-id:ff80818178a0d4210178a0d85fae0002}")
    private String walletClaimId;

    private NbCashWalletInterface nbCashWalletInterface;

    public NbCashOptionProcess(@Value("${spring.scratch-card.nb-cash.base-url:https://www.nobroker.in/wallet-api/}") String baseUrl) {
        Retrofit retrofit = RetrofitUtil.getRetrofit(baseUrl, true);
        nbCashWalletInterface = retrofit.create(NbCashWalletInterface.class);
    }


    @Override
    @Transactional
    public boolean processOption(ProcessOptionModel processOptionModel) {
        ScratchCardPayload scratchCardPayload = processOptionModel.getScratchCardPayload();
        ContestDetail contestDetail = processOptionModel.getContestDetail();
        BigDecimal minNbCashValue = contestDetail.getMinNbCashValue();
        BigDecimal maxNbCashValue = contestDetail.getMaxNbCashValue();
        OptionJson optionJson = processOptionModel.getOptionJson();
        String dailyNbCashLimit = optionJson.getDailyNbCashLimit();
        Optional<String> dailyNbCashCount = redisService.getValueFromCache(ScratchCardConstants.DAILYNBCASHCOUNT.concat(contestDetail.getId()));
        if (dailyNbCashCount.isPresent() && dailyNbCashLimit != null) {
            if (Integer.parseInt(dailyNbCashLimit) <= Integer.parseInt(dailyNbCashCount.get())) {
                logger.info("daily nb cash limit limit exceeds for the contest {} reference id {} ", contestDetail.getId(), processOptionModel.getScratchCardPayload().getReferenceId());
                return false;
            }
        }
        int cashBackAmount = ScratchCardUtils.getRandomValueInRange(minNbCashValue.intValue(), maxNbCashValue.intValue());
        CashBack cashBack = saveCashBack(cashBackAmount, contestDetail);
        scratchCardPayload.setOfferId(cashBack.getId());
        scratchCardPayload.setOfferType(OfferType.NB_CASH);
        scratchCardPayload.setExpiryDate(ScratchCardUtils.addMonths(new Date(), nbCashExpiry));
        scratchCardPayload.setScratchCardState(processOptionModel.getScratchCardState());
        scratchCardPayload.setContestDetail(contestDetail);
        scratchCardService.saveScratchCard(scratchCardPayload);
        if (dailyNbCashCount.isPresent()) {
            redisService.setKeyValueWithExpiry(ScratchCardConstants.DAILYNBCASHCOUNT.concat(contestDetail.getId()), String.valueOf(Integer.parseInt(dailyNbCashCount.get()) + 1), 1);
        } else {
            redisService.setKeyValueWithExpiry(ScratchCardConstants.DAILYNBCASHCOUNT.concat(contestDetail.getId()), String.valueOf(1), 1);

        }
        return true;
    }

    private CashBack saveCashBack(int cashBackAmount, ContestDetail contestDetail) {
        CashBack cashBack = new CashBack();
        cashBack.setAmount(new BigDecimal(cashBackAmount));
        cashBack.setCashBackState(CashBackState.INITIATED);
        cashBack.setLastUpdatedDate(new Date());
        cashBack.setCreatedDate(new Date());
        cashBack.setCashTransactionType(CashTransactionType.NB_CASH);
        cashBack.setContestDetail(contestDetail);
        cashBackService.save(cashBack);
        return cashBack;
    }

    @Override
    public void postProcessScratchCard(ScratchCardInfo scratchCardInfo) {
        CashBack cashBack = scratchCardInfo.getCashBack();
        ScratchCard scratchCard = scratchCardInfo.getScratchCard();
        UserDetail userDetail = scratchCard.getUserDetail();
        logger.info("post process nb cash user id {} ref id {}", userDetail.getUserId(), scratchCard.getReferenceId());
        LocalDateTime localDateTime = LocalDateTime.now().plusMonths(nbCashWalletExpiry);
        String localTime = localDateTime.format(DateTimeFormatter.ofPattern(ScratchCardConstants.DATE_FORMAT));
        WalletCashTopupForm walletCashTopupForm = WalletCashTopupForm.builder().amount(String.valueOf(cashBack.getAmount()))
                .claimId(walletClaimId)
                .expiryDate(localTime)
                .source(CashSourceType.REWARD)
                .userId(userDetail.getUserId())
                .mobileNumber(userDetail.getMobileNumber())
                .email(userDetail.getEmail())
                .clientRefId(scratchCard.getReferenceId())
                .build();
//        scratchCard.setScratchCardState(ScratchCardState.SCRATCHED);
//        scratchCard.setLastUpdatedDate(new Date());
//        scratchCardService.save(scratchCard);
        Call<ResponseDTO<NbCashTopUpRespose>> responseEntityCall = nbCashWalletInterface.topUp(walletApiKeyValue, walletCashTopupForm);
        try {
            Response<ResponseDTO<NbCashTopUpRespose>> responseEntityResponse = responseEntityCall.execute();
            ResponseDTO<NbCashTopUpRespose> body = responseEntityResponse.body();
            if (responseEntityResponse.code() == 200) {
                logger.info("reference id {} user id {} body of response {}", scratchCard.getReferenceId(), userDetail.getUserId(), JSONUtil.getStringFromObject(body));
                if (body != null) {
                    NbCashTopUpRespose data = body.getData();
                    cashBack.setTransactionId(data.getId());
                    cashBack.setCashBackState(CashBackState.SUCCESS);
                    cashBack.setLastUpdatedDate(new Date());
                    cashBackService.save(cashBack);
                }
            } else {
                logger.error("reference id {} user id {} body top up error response code  {}", scratchCard.getReferenceId(), userDetail.getUserId(), responseEntityResponse.code());

            }

        } catch (Exception e) {
            logger.error("exception in calling wallet top up api {} ref id {} user id {} ", e.getMessage(), scratchCard.getReferenceId(), userDetail.getUserId());
        }
    }
}
