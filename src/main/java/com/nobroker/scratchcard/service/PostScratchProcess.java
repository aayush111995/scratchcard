package com.nobroker.scratchcard.service;

/**
 * Aayush Chaudhary
 */
public interface PostScratchProcess<T> {

    void postProcessScratchCard(T t);
}
