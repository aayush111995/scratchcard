package com.nobroker.scratchcard.service;

import com.nobroker.scratchcard.enums.Options;
import com.nobroker.scratchcard.enums.ScratchCardRandomStrategyName;
import com.nobroker.scratchcard.model.OptionJson;

/**
 * Aayush Chaudhary
 */
public interface ScratchCardRandomStrategy {

    Options pickRandomOption(OptionJson optionJson);

    ScratchCardRandomStrategyName getScratchCardRandomStrategyName();
}
