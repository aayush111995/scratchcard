package com.nobroker.scratchcard.service;

import com.nobroker.scratchcard.dao.CouponCodeRepository;
import com.nobroker.scratchcard.entity.Client;
import com.nobroker.scratchcard.entity.CouponCode;
import com.nobroker.scratchcard.entity.CouponDetail;
import com.nobroker.scratchcard.model.CouponCodeModel;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Aayush Chaudhary
 */
@Service
@AllArgsConstructor
public class CouponCodeService {
    public static Logger logger = LoggerFactory.getLogger(CouponCodeService.class);


    private CouponCodeRepository couponCodeRepository;
    private CouponDetailService couponDetailService;
    private ClientService clientService;


    public Optional<CouponCode> findById(String id) {
        return couponCodeRepository.findById(id);
    }

    public List<CouponCode> findAll() {
        return couponCodeRepository.findAll();
    }

    public List<CouponCode> getByClientIdAndCouponDetailId(String clientId, List<String> couponDetailId, Pageable pageable) {
        return couponCodeRepository.findByContestIdAndClientIdAndCouponDetailId(clientId, couponDetailId, pageable);
    }

    public List<CouponCode> getActiveCoupons(boolean isActive) {
        return couponCodeRepository.findByIsActiveAndExpiry(isActive);
    }

    @Transactional
    public void saveCouponCode(CouponCode couponCode) {
        couponCodeRepository.save(couponCode);
    }

    @Transactional
    public int loadCouponCodes(List<CouponCodeModel> couponCodeModelList) {
        List<CouponCode> couponCodes = new ArrayList<>();
        for (CouponCodeModel couponCodeModel : couponCodeModelList) {
            try {
                Optional<CouponDetail> optionalCouponDetail = couponDetailService.findById(couponCodeModel.getCouponDetailId());
                Optional<Client> optionalClient = clientService.findById(couponCodeModel.getClientId());
                CouponCode couponCode = new CouponCode();
                couponCode.setCouponType(couponCodeModel.getCouponType());

                couponCode.setCouponDetail(optionalCouponDetail.orElse(null));
                couponCode.setCouponProvider(couponCodeModel.getCouponProvider());
                couponCode.setIsActive(couponCodeModel.getIsActive());
                couponCode.setClient(optionalClient.orElse(null));
                couponCode.setCreatedDate(new Date());
                couponCode.setLastUpdatedDate(new Date());
                couponCode.setExpiryDate(couponCodeModel.getExpiryDate());
                couponCode.setId(couponCodeModel.getId());
                saveCouponCode(couponCode);
                couponCodes.add(couponCode);
            } catch (Exception exception) {
                logger.error("exception in loading the coupon code in db id {}  contest id {} coupon detail id {} client id {} exception {}", couponCodeModel.getId(), couponCodeModel.getContestDetailId(), couponCodeModel.getCouponDetailId(), couponCodeModel.getClientId(), exception);
            }
        }
        return couponCodes.size();
    }


}
