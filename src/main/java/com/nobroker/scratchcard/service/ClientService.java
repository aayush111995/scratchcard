package com.nobroker.scratchcard.service;

import com.nobroker.scratchcard.dao.ClientRepository;
import com.nobroker.scratchcard.entity.Client;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Aayush Chaudhary
 */
@Service
@AllArgsConstructor
public class ClientService {

    private final ClientRepository clientRepository;

    public Optional<Client> findById(String id) {
        return clientRepository.findById(id);
    }
}
