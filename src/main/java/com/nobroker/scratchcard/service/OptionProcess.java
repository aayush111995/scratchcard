package com.nobroker.scratchcard.service;

/**
 * Aayush Chaudhary
 */
public interface OptionProcess<T> {

    boolean processOption(T t);
}
