package com.nobroker.scratchcard.service;

import com.nobroker.scratchcard.dao.CashBackRepository;
import com.nobroker.scratchcard.entity.CashBack;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * Aayush Chaudhary
 */
@Service
@AllArgsConstructor
public class CashBackService {

    private final CashBackRepository cashBackRepository;

    @Transactional
    public void save(CashBack cashBack) {
        cashBackRepository.save(cashBack);
    }

    public Optional<CashBack> findById(String id) {
        return cashBackRepository.findById(id);
    }
}
