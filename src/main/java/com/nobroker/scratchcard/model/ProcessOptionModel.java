package com.nobroker.scratchcard.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.nobroker.scratchcard.entity.ContestDetail;
import com.nobroker.scratchcard.enums.ScratchCardState;
import lombok.Data;

/**
 * Aayush Chaudhary
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProcessOptionModel {

    private ContestDetail contestDetail;
    private ScratchCardPayload scratchCardPayload;
    private ScratchCardState scratchCardState;
    private OptionJson optionJson;
}
