package com.nobroker.scratchcard.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * Aayush Chaudhary
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
public class WalletCashTopupForm implements Serializable {
    private String userId;
    private String amount;
    private String expiryDate;
    private String clientRefId;
    private CashSourceType source;
    private String claimId;
    private String mobileNumber;
    private String email;


}
