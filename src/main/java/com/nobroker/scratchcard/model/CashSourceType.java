package com.nobroker.scratchcard.model;

/**
 * Aayush Chaudhary
 */
public enum CashSourceType {
    CAMPAIGN,
    REWARD
}
