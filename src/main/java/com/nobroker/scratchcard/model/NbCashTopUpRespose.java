package com.nobroker.scratchcard.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Aayush Chaudhary
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NbCashTopUpRespose {
    private String id;
    private String clientRefId;
    private String type;
    private String transactionSource;
    private BigDecimal amount;
    private String transactionStatus;
    private String createdDate;


}
