package com.nobroker.scratchcard.model;


import lombok.Builder;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * Aayush Chaudhary
 */
@Data
@Builder
public class ResponseDTO<T> {

    private final Map<String, Object> otherParams = new HashMap<String, Object>();
    @Builder.Default
    private Integer status = 200;
    @Builder.Default
    private Integer statusCode = 200;
    @Builder.Default
    private String message = "success";
    private T data;

}
