package com.nobroker.scratchcard.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.nobroker.scratchcard.enums.CouponCompany;
import com.nobroker.scratchcard.enums.CouponType;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Aayush Chaudhary
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CouponCodeModel {

    @NotNull
    private String id;
    @NotNull
    private String couponDetailId;
    @NotNull
    private String clientId;
    private Date expiryDate;
    private Boolean isActive;
    @NotNull
    private CouponCompany couponProvider;
    @NotNull
    private CouponType couponType;
    @NotNull
    private String contestDetailId;
    private Date createdDate;
    private Date lastUpdatedDate;

}
