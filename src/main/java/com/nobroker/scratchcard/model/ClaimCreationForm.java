package com.nobroker.scratchcard.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * Aayush Chaudhary
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
public class ClaimCreationForm implements Serializable {
    private String userId;
    private String claimId;
    private String mobileNumber;
    private String email;
    private String referenceId;
}
