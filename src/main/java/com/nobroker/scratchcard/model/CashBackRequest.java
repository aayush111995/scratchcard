package com.nobroker.scratchcard.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.nobroker.scratchcard.enums.CashBackState;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * Aayush Chaudhary
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CashBackRequest {

    @NotNull
    private String cashBackId;

    @NotNull
    private CashBackState cashBackState;
    @NotNull
    private String transactionId;
}
