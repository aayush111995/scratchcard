package com.nobroker.scratchcard.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.nobroker.scratchcard.enums.UserSource;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * Aayush Chaudhary
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserInfo {

    @NotNull
    private String userId;
    private String userName;
    private String mobileNumber;
    private String email;
    private UserSource userSource;
}
