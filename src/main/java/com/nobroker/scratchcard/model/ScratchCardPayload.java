package com.nobroker.scratchcard.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.nobroker.scratchcard.entity.ContestDetail;
import com.nobroker.scratchcard.entity.UserDetail;
import com.nobroker.scratchcard.enums.OfferType;
import com.nobroker.scratchcard.enums.ScratchCardState;
import lombok.Data;

import java.util.Date;

/**
 * Aayush Chaudhary
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ScratchCardPayload {
    private String referenceId;
    private String flowName;
    private String client;
    private UserDetail userDetail;
    private String offerId;
    private OfferType offerType;
    private ScratchCardState scratchCardState;
    private Date expiryDate;
    private ContestDetail contestDetail;

}
