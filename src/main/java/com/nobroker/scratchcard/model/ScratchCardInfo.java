package com.nobroker.scratchcard.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.nobroker.scratchcard.entity.CashBack;
import com.nobroker.scratchcard.entity.CouponCode;
import com.nobroker.scratchcard.entity.ScratchCard;
import lombok.Data;

/**
 * Aayush Chaudhary
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ScratchCardInfo {

    private ScratchCard scratchCard;
    private CouponCode couponCode;
    private CashBack cashBack;
    private String uiJson;
}
