package com.nobroker.scratchcard.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * Aayush Chaudhary
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CouponContestModel {

    @NotNull
    private String couponDetailId;
    @NotNull
    private String contestId;
}
