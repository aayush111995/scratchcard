package com.nobroker.scratchcard.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.nobroker.scratchcard.entity.ContestDetail;
import com.nobroker.scratchcard.entity.CouponCode;
import com.nobroker.scratchcard.entity.ScratchCard;
import lombok.Data;

/**
 * Aayush Chaudhary
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ScratchCardModel {

    private ScratchCard scratchCard;
    private ContestDetail contestDetail;
    private CouponCode couponCode;
}
