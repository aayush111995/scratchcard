package com.nobroker.scratchcard.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * Aayush Chaudhary
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CouponDetailModel {

    @NotNull
    private String id;
    @NotNull
    private CouponDetailJson couponDetailJson;

    @Data
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class CouponDetailJson {

        private String backgroundImage;
        private String partner;
        private String partnerLogo;
        private String category;
        private String title;
        private String description;
        private String cityValidity;
        private String expireContent;
        private String expireOn;

    }

}
