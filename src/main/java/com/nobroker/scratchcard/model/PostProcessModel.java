package com.nobroker.scratchcard.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.nobroker.scratchcard.entity.CashBack;
import com.nobroker.scratchcard.entity.ScratchCard;
import lombok.Data;

/**
 * Aayush Chaudhary
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PostProcessModel {
    private ScratchCard scratchCard;
    private CashBack cashBack;
}
