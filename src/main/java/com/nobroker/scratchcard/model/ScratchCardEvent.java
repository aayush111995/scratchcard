package com.nobroker.scratchcard.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * Aayush Chaudhary
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ScratchCardEvent {
}
