package com.nobroker.scratchcard.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * Aayush Chaudhary
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OptionJson {

    private String coupon;
    private String nbCash;
    private String cashBack;
    private String betterLuckNextTime;
    private OptionJson couponFallBack;
    private OptionJson nbCashFallBack;
    private OptionJson cashBackFallBack;
    private String dailyCouponLimit;
    private String dailyCashBackLimit;
    private String dailyNbCashLimit;
}
