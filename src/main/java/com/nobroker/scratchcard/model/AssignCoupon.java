package com.nobroker.scratchcard.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Aayush Chaudhary
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AssignCoupon {

    @NotNull
    private String scratchCardId;
    @NotNull
    private List<String> couponDetailIds;
    @NotNull
    private String clientId;
}
