package com.nobroker.scratchcard.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * Aayush Chaudhary
 */
@Data
@AllArgsConstructor
public class ApiError {
    private List<String> errors;
}
