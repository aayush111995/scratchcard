package com.nobroker.scratchcard.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.nobroker.scratchcard.enums.ScratchCardEventType;
import lombok.Data;

import javax.validation.Valid;

/**
 * Aayush Chaudhary
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ScratchCardEventPayload {
    @Valid
    private UserInfo userInfo;
    private ScratchCardEventType scratchCardEventType;
    private String referenceId;
    private String flowName;
    private String client;
}
