package com.nobroker.scratchcard.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Aayush Chaudhary
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
public class RealCashTopup implements Serializable {
    private String refId;
    private BigDecimal amount;
    private String type;
}
