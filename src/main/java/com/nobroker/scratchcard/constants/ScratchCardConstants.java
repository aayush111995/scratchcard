package com.nobroker.scratchcard.constants;

/**
 * Aayush Chaudhary
 */
public class ScratchCardConstants {

    public static final String SCRATCH_CARD_CACHE = "scratch_card_cache";


    public static final String INITIATE = "INITIATE";
    public static final String INITIATE_AND_SUCCESS = "INITIATE_AND_SUCCESS";
    public static final String SUCCESS = "SUCCESS";
    public static final String COUPON = "COUPON";
    public static final String NBCASH = "NBCASH";
    public static final String CASHBACK = "CASHBACK";
    public static final String BETTERLUCK = "BETTERLUCK";
    public static final String DAILYUSERCOUNT = "DAILYUSERCOUNT";
    public static final String DAILYCOUPONCOUNT = "DAILYCOUPONCOUNT";
    public static final String DAILYCASHBACKCOUNT = "DAILYCASHBACKCOUNT";
    public static final String DAILYNBCASHCOUNT = "DAILYNBCASHCOUNT";
    public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    public static final String ALL_COUPON_IDS = "ALL";
    public static final int MaxCouponResults = 1;
    public static final String REAL_TOP_UP_TYPE = "CO_1000";


}
