package com.nobroker.scratchcard.dao;

import com.nobroker.scratchcard.entity.ScratchCard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Aayush Chaudhary
 */
@Repository
public interface ScratchCardRepository extends JpaRepository<ScratchCard, String> {

    Optional<ScratchCard> findByReferenceId(String refId);

    @Query("select c from ScratchCard c inner join c.userDetail cd where cd.userId=(:userId) order by c.expiryDate asc ,c.lastUpdatedDate desc")
    List<ScratchCard> findByUserId(String userId);
}
