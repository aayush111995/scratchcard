package com.nobroker.scratchcard.dao;

import com.nobroker.scratchcard.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Aayush Chaudhary
 */
@Repository
public interface ClientRepository extends JpaRepository<Client, String> {
}
