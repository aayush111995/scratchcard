package com.nobroker.scratchcard.dao;

import com.nobroker.scratchcard.entity.ContestDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Aayush Chaudhary
 */
@Repository
public interface ContestDetailRepository extends JpaRepository<ContestDetail, String> {

    List<ContestDetail> findByFlowName(String flowName);
}
