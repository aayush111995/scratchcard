package com.nobroker.scratchcard.dao;

import com.nobroker.scratchcard.entity.UserDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Aayush Chaudhary
 */
@Repository
public interface UserDetailRepository extends JpaRepository<UserDetail, String> {
}
