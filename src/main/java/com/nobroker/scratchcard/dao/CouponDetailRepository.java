package com.nobroker.scratchcard.dao;

import com.nobroker.scratchcard.entity.CouponDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Aayush Chaudhary
 */
@Repository
public interface CouponDetailRepository extends JpaRepository<CouponDetail, String> {


    List<CouponDetail> findByIdIn(List<String> ids);
}
