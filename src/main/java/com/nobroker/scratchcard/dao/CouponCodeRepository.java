package com.nobroker.scratchcard.dao;

import com.nobroker.scratchcard.entity.CouponCode;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Aayush Chaudhary
 */
@Repository
public interface CouponCodeRepository extends JpaRepository<CouponCode, String> {


    @Query("select c from CouponCode c inner join c.couponDetail ccd inner join c.client cl where cl.id=(:clientId) and ccd.id in (:couponDetailId) and c.isActive=true and c.expiryDate> NOW() order by c.expiryDate ASC")
    List<CouponCode> findByContestIdAndClientIdAndCouponDetailId(String clientId, List<String> couponDetailId, Pageable pageable);

    @Query("select c from CouponCode c  where c.isActive=(:isActive) and c.expiryDate> NOW() order by c.expiryDate ASC")
    List<CouponCode> findByIsActiveAndExpiry(boolean isActive);
}
