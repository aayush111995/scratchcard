package com.nobroker.scratchcard.dao;

import com.nobroker.scratchcard.entity.CouponContest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Aayush Chaudhary
 */
@Repository
public interface CouponContestRepository extends JpaRepository<CouponContest, String> {


    List<CouponContest> findByContestId(String contestId);
}
