package com.nobroker.scratchcard.dao;

import com.nobroker.scratchcard.entity.CashBack;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Aayush Chaudhary
 */
@Repository
public interface CashBackRepository extends JpaRepository<CashBack, String> {

}
