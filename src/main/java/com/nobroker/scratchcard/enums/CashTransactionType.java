package com.nobroker.scratchcard.enums;

/**
 * Aayush Chaudhary
 */
public enum CashTransactionType {
    NB_CASH, CASH_BACK
}
