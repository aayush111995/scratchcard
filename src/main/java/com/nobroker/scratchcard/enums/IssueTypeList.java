

package com.nobroker.scratchcard.enums;


/**
 * @author Aayush Chaudhary
 */
public enum IssueTypeList {


    required("required"),

    value("value"),

    forbidden("forbidden"),

    duplicate("duplicate"),
    security("security"),
    exception("exception");

    private final String value1;

    IssueTypeList(String v) {
        value1 = v;
    }

    public static IssueTypeList fromValue(String v) {
        for (IssueTypeList c : IssueTypeList.values()) {
            if (c.value1.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

    public String value() {
        return value1;
    }

}
