package com.nobroker.scratchcard.enums;

/**
 * Aayush Chaudhary
 */
public enum CouponType {
    DEAL, COUPON
}
