package com.nobroker.scratchcard.enums;

/**
 * Aayush Chaudhary
 */
public enum ScratchCardState {
    LOCKED, UNLOCKED, SCRATCHED, ASSIGNED
}
