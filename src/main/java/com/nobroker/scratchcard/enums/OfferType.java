package com.nobroker.scratchcard.enums;

/**
 * Aayush Chaudhary
 */
public enum OfferType {
    CASHBACK, COUPON, NB_CASH, BETTER_LUCK
}
