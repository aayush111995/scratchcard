package com.nobroker.scratchcard.enums;

/**
 * Aayush Chaudhary
 */
public enum CouponCompany {
    NOBROKER, AMAZON, MYNTRA
}
