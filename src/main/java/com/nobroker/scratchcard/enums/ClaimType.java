package com.nobroker.scratchcard.enums;

/**
 * Aayush Chaudhary
 */
public enum ClaimType {
    CLAIM,
    REWARD,
    CAMPAIGN
}
