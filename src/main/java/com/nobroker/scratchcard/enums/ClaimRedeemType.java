package com.nobroker.scratchcard.enums;

/**
 * Aayush Chaudhary
 */
public enum ClaimRedeemType {
    PERCENTAGE,
    FLAT
}
