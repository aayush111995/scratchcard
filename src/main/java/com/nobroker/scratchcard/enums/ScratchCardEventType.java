package com.nobroker.scratchcard.enums;

/**
 * Aayush Chaudhary
 */
public enum ScratchCardEventType {
    INITIATE_AND_SUCCESS, SUCCESS, INITIATE
}
