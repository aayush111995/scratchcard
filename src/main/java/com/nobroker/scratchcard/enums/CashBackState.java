package com.nobroker.scratchcard.enums;

/**
 * Aayush Chaudhary
 */
public enum CashBackState {
    SUCCESS, INITIATED, PROCESSING
}
