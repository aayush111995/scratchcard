
package com.nobroker.scratchcard.enums;

/**
 * @author Aayush Chaudhary
 */
public enum IssueSeverityList {

    fatal("fatal"),

    error("error"),

    warning("warning"),

    information("information");
    private final String value;

    IssueSeverityList(String v) {
        value = v;
    }

    public static IssueSeverityList fromValue(String v) {
        for (IssueSeverityList c : IssueSeverityList.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

    public String value() {
        return value;
    }

}
