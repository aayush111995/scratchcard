package com.nobroker.scratchcard.enums;

/**
 * Aayush Chaudhary
 */
public enum ScratchCardRandomStrategyName {
    MODULUS_PICK,
    RANDOM_PICK
}
