package com.nobroker.scratchcard.enums;

/**
 * Aayush Chaudhary
 */
public enum Options {
    COUPON, CASHBACK, NBCASH, BETTERLUCK
}
