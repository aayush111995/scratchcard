package com.nobroker.scratchcard.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.nobroker.scratchcard.constants.ScratchCardConstants;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Aayush Chaudhary
 */
public class LocalDateTimeSerializer extends StdSerializer<LocalDateTime> {

    /**
     *
     */
    private static final long serialVersionUID = -560022391681265733L;


    public LocalDateTimeSerializer() {
        super(LocalDateTime.class);
    }

    @Override
    public void serialize(LocalDateTime dateTime, JsonGenerator generator, SerializerProvider provider) throws IOException {
        generator.writeString(dateTime.format(DateTimeFormatter.ofPattern(ScratchCardConstants.DATE_FORMAT)));
    }
}