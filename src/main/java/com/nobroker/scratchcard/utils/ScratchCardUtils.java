package com.nobroker.scratchcard.utils;

import java.util.Calendar;
import java.util.Date;

/**
 * Aayush Chaudhary
 */
public class ScratchCardUtils {

    public static int getRandomValueInRange(int min, int max) {
        int range = max - min + 1;
        return (int) (Math.random() * range) + min;
    }

    public static Date addMonths(Date date, int months) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, months);
        return cal.getTime();
    }

    public static String getFrontEndJsonForCashBack() {
        return "{\"backgroundImage\": \"https://assets.nobroker.in/nb-new/public/Wallet/cashback.png\"}";
    }

    public static String getFrontEndJsonForNbCash() {
        return "{\"backgroundImage\": \"https://assets.nobroker.in/nb-new/public/Wallet/cashback.png\"}";
    }

    public static String getFrontEndJsonForBetterLuck() {
        return "{\"backgroundImage\": \"https://assets.nobroker.in/nb-new/public/Wallet/better_luck.png\"}";
    }

}
