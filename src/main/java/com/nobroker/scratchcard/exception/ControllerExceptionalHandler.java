package com.nobroker.scratchcard.exception;

import com.nobroker.scratchcard.model.ApiError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;

/**
 * Aayush Chaudhary
 */
@ControllerAdvice
public class ControllerExceptionalHandler {

    protected static final Logger LOG = LoggerFactory.getLogger(ControllerExceptionalHandler.class);

    @ExceptionHandler({BadCredentialsException.class})
    public final ResponseEntity<?> handleException(Exception ex, WebRequest request) {
        HttpHeaders headers = new HttpHeaders();
        List<String> bad_credentials = Collections.singletonList("Bad credentials");
        return new ResponseEntity<>(new ApiError(bad_credentials), headers, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler
    public ModelAndView defaultErrorHandler(HttpServletRequest request, Throwable e) {

        LOG.error("Error occured: ", e);
        ModelAndView mav = new ModelAndView("redirect:/error/500.html");
        return mav;
    }
}
