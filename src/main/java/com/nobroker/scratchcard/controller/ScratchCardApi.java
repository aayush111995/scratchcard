package com.nobroker.scratchcard.controller;

import com.nobroker.scratchcard.entity.*;
import com.nobroker.scratchcard.kafka.KafkaProducer;
import com.nobroker.scratchcard.model.*;
import com.nobroker.scratchcard.service.*;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Aayush Chaudhary
 */
@RestController
@AllArgsConstructor
@RequestMapping("/v1")
public class ScratchCardApi {

    private final KafkaProducer kafkaProducer;
    private final CardService cardService;
    private final CouponCodeService couponCodeService;
    private final ScratchCardService scratchCardService;
    public static Logger logger = LoggerFactory.getLogger(ScratchCardApi.class);
    private final CouponDetailService couponDetailService;
    private final CouponContestService couponContestService;
    private final CashBackService cashBackService;


    @RequestMapping(method = RequestMethod.POST, value = "/send-request", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO<Object>> sendScratchCardEventTest(@RequestBody ScratchCardEventPayload scratchCardEventPayload) throws Exception {
        kafkaProducer.produceTestScratchCardEvent(scratchCardEventPayload);
        ResponseDTO<Object> responseDTO = ResponseDTO.builder().data("success").build();
        return ResponseEntity.status(HttpStatus.OK).body(responseDTO);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/do-card-scratch", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO<Object>> getScratchCardInfo(@RequestParam("scratchCardId") String scratchCardId) {
        ScratchCardInfo scratchCardInfo = cardService.getScratchCardInfoAndPostProcess(scratchCardId);
        ResponseDTO<Object> responseDTO = ResponseDTO.builder().data(scratchCardInfo).build();
        return ResponseEntity.status(HttpStatus.OK).body(responseDTO);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/get-scratch-card", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO<Object>> getScratchCardViaReferenceId(@RequestParam("referenceId") String referenceId) {
        Optional<ScratchCard> scratchCard = scratchCardService.findScratchCardByRefId(referenceId);
        ResponseDTO<Object> responseDTO = ResponseDTO.builder().data(scratchCard.orElse(null)).build();
        return ResponseEntity.status(HttpStatus.OK).body(responseDTO);
    }

    @Validated
    @RequestMapping(method = RequestMethod.POST, value = "/assign-coupon-code", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO<Object>> assignCouponCode(@RequestBody @Valid AssignCoupon assignCoupon) {
        ScratchCardInfo scratchCardInfo = cardService.assignCouponCode(assignCoupon);
        ResponseDTO<Object> responseDTO = ResponseDTO.builder().data(scratchCardInfo).build();
        return ResponseEntity.status(HttpStatus.OK).body(responseDTO);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/fetch/coupon/{coupon-id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO<Object>> getCoupon(@PathVariable("coupon-id") String couponId) {
        ResponseDTO<Object> responseDTO;
        Optional<CouponCode> optionalCouponCode = couponCodeService.findById(couponId);
        if (optionalCouponCode.isPresent()) {
            responseDTO = ResponseDTO.builder().data(optionalCouponCode.get()).build();
            return ResponseEntity.status(HttpStatus.OK).body(responseDTO);

        } else {
            responseDTO = ResponseDTO.builder().data("coupon id not found").build();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseDTO);

        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/fetch/all/coupon")
    public ResponseEntity<ResponseDTO<Object>> getAllCoupons() {
        ResponseDTO<Object> responseDTO;
        List<CouponCode> couponCodeList = couponCodeService.findAll();
        responseDTO = ResponseDTO.builder().data(couponCodeList).build();
        return ResponseEntity.status(HttpStatus.OK).body(responseDTO);

    }

    @RequestMapping(method = RequestMethod.GET, value = "/fetch/all/scratch-card/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO<Object>> getAllUserActivity(@PathVariable("userId") String userId) {
        ResponseDTO<Object> responseDTO;
        List<ScratchCardInfo> scratchCardInfos = cardService.getAllScratchCardOfUser(userId);
        responseDTO = ResponseDTO.builder().data(scratchCardInfos).build();
        return ResponseEntity.status(HttpStatus.OK).body(responseDTO);

    }

    @RequestMapping(method = RequestMethod.GET, value = "/fetch/all/coupon/details", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO<Object>> getAllCouponDetails() {
        ResponseDTO<Object> responseDTO;
        List<CouponDetail> allCouponDetails = couponDetailService.getAllCouponDetailsWithCashBackAndNbCash();
        responseDTO = ResponseDTO.builder().data(allCouponDetails).build();
        return ResponseEntity.status(HttpStatus.OK).body(responseDTO);

    }

    @RequestMapping(method = RequestMethod.POST, value = "/load/coupon/codes", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO<Object>> loadCoupons(@RequestBody List<CouponCodeModel> couponCodeModelList) {
        ResponseDTO<Object> responseDTO;
        int couponCodesSize = couponCodeService.loadCouponCodes(couponCodeModelList);
        responseDTO = ResponseDTO.builder().data(couponCodesSize).build();
        return ResponseEntity.status(HttpStatus.OK).body(responseDTO);

    }


    @RequestMapping(method = RequestMethod.POST, value = "/load/coupon/details", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO<Object>> loadCouponDetails(@RequestBody List<CouponDetailModel> couponDetailList) {
        ResponseDTO<Object> responseDTO;
        int couponCodesSize = couponDetailService.loadCouponDetails(couponDetailList);
        responseDTO = ResponseDTO.builder().data(couponCodesSize).build();
        return ResponseEntity.status(HttpStatus.OK).body(responseDTO);

    }

    @RequestMapping(method = RequestMethod.POST, value = "/load/coupon/contest", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO<Object>> loadCouponContestMap(@RequestBody List<CouponContestModel> couponContestModelList) {
        ResponseDTO<Object> responseDTO;
        int couponContestSize = couponContestService.loadCouponContestMap(couponContestModelList);
        responseDTO = ResponseDTO.builder().data(couponContestSize).build();
        return ResponseEntity.status(HttpStatus.OK).body(responseDTO);

    }


    @RequestMapping(method = RequestMethod.GET, value = "/fetch/contest/coupon/detail/{contest-id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO<Object>> fetchCoupnDetailViaContest(@PathVariable("contest-id") String contestId) {
        ResponseDTO<Object> responseDTO;
        List<CouponContest> couponContestList = couponContestService.findByContestId(contestId);
        List<CouponDetail> couponDetails = couponDetailService.getCouponDetailsFromCouponContest(couponContestList);
        responseDTO = ResponseDTO.builder().data(couponDetails).build();
        return ResponseEntity.status(HttpStatus.OK).body(responseDTO);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/fetch/cashback/{cashback-id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO<Object>> fetchCashBackViaId(@PathVariable("cashback-id") String cashBackId) {
        ResponseDTO<Object> responseDTO;
        Optional<CashBack> optionalCashBack = cashBackService.findById(cashBackId);

        responseDTO = ResponseDTO.builder().data(optionalCashBack.orElse(null)).build();
        return ResponseEntity.status(HttpStatus.OK).body(responseDTO);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/fetch/active/coupons", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO<Object>> fetchAllActiveCoupons() {
        ResponseDTO<Object> responseDTO;
        List<CouponCode> activeCoupons = couponCodeService.getActiveCoupons(true);
        responseDTO = ResponseDTO.builder().data(activeCoupons).build();
        return ResponseEntity.status(HttpStatus.OK).body(responseDTO);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/update/cash/back", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO<Object>> updateCashBackResponse(@RequestBody CashBackRequest cashBackRequest) {
        ResponseDTO<Object> responseDTO;
        Optional<CashBack> optionalCashBack = cashBackService.findById(cashBackRequest.getCashBackId());
        if (optionalCashBack.isPresent()) {
            CashBack cashBack = optionalCashBack.get();
            cashBack.setCashBackState(cashBackRequest.getCashBackState());
            cashBack.setTransactionId(cashBackRequest.getTransactionId());
            cashBack.setLastUpdatedDate(new Date());
            cashBackService.save(cashBack);
        }
        responseDTO = ResponseDTO.builder().data(optionalCashBack.orElse(null)).build();
        return ResponseEntity.status(HttpStatus.OK).body(responseDTO);
    }


}
