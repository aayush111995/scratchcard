package com.nobroker.scratchcard.kafka;

import com.nobroker.scratchcard.model.ScratchCardEventPayload;
import com.nobroker.scratchcard.model.ScratchCardInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

/**
 * Aayush Chaudhary
 */
@Service
public class KafkaProducer {

    public static Logger logger = LoggerFactory.getLogger(KafkaProducer.class);

    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;
    @Value(value = "${spring.kafka.topic.scratch-card-event}")
    private String scratchCardEvent;

    @Value(value = "${spring.kafka.topic.post-scratch-card-event}")
    private String postScratchCardEvent;


    public void produceTestScratchCardEvent(ScratchCardEventPayload scratchCardEventPayload) {
        this.kafkaTemplate.send(scratchCardEvent, scratchCardEventPayload.getReferenceId(), scratchCardEventPayload);
    }

    public void producePostScratchCardEvent(ScratchCardInfo scratchCardInfo) {
        this.kafkaTemplate.send(postScratchCardEvent, scratchCardInfo.getScratchCard().getId(), scratchCardInfo);
    }

}
