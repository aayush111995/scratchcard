package com.nobroker.scratchcard.kafka;

import com.nobroker.scratchcard.constants.ScratchCardConstants;
import com.nobroker.scratchcard.entity.ScratchCard;
import com.nobroker.scratchcard.entity.UserDetail;
import com.nobroker.scratchcard.enums.ScratchCardEventType;
import com.nobroker.scratchcard.factory.OptionProcessFactory;
import com.nobroker.scratchcard.factory.ScratchCardEventFactory;
import com.nobroker.scratchcard.listener.ScratchCardEvent;
import com.nobroker.scratchcard.model.ScratchCardEventPayload;
import com.nobroker.scratchcard.model.ScratchCardInfo;
import com.nobroker.scratchcard.model.ScratchCardPayload;
import com.nobroker.scratchcard.redis.RedisService;
import com.nobroker.scratchcard.service.PostScratchProcess;
import com.nobroker.scratchcard.service.UserDetailService;
import com.nobroker.scratchcard.utils.JSONUtil;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Optional;
import java.util.Set;

/**
 * Aayush Chaudhary
 */
@Service
public class ScratchCardKafkaListener {
    public static Logger logger = LoggerFactory.getLogger(ScratchCardKafkaListener.class);


    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;

    @Autowired
    private UserDetailService userDetailService;

    @Autowired
    private RedisService redisService;

    @Autowired
    private Validator validator;


    @KafkaListener(topics = "${spring.kafka.topic.scratch-card-event}", groupId = "${spring.kafka.consumer.group-id}")
    public void consumeScratchCardEvent(ConsumerRecord<String, ScratchCardEventPayload> cr,
                                        @Payload ScratchCardEventPayload scratchCardEventPayload) {
        Set<ConstraintViolation<ScratchCardEventPayload>> violations = validator.validate(scratchCardEventPayload);
        for (ConstraintViolation<ScratchCardEventPayload> violation : violations) {
            logger.error("violation in the consuming event payload {}, path {}", violation.getMessage(), violation.getPropertyPath());
            return;
        }
        try {
            logger.info("user id for card event {} payload of event {}", scratchCardEventPayload.getUserInfo().getUserId(), JSONUtil.getStringFromObject(scratchCardEventPayload));
            if (scratchCardEventPayload.getScratchCardEventType() == ScratchCardEventType.INITIATE || scratchCardEventPayload.getScratchCardEventType() == ScratchCardEventType.INITIATE_AND_SUCCESS) {
                Optional<String> dailyUserCount = redisService.getValueFromCache(ScratchCardConstants.DAILYUSERCOUNT);
                if (dailyUserCount.isPresent()) {
                    redisService.setKeyValueWithExpiry(ScratchCardConstants.DAILYUSERCOUNT, String.valueOf(Integer.parseInt(dailyUserCount.get()) + 1), 1);
                } else {
                    redisService.setKeyValueWithExpiry(ScratchCardConstants.DAILYUSERCOUNT, String.valueOf(1), 1);

                }
            }
            UserDetail userDetail = userDetailService.saveUserInfoIfNotExistsEvent(scratchCardEventPayload);
            ScratchCardEvent<ScratchCardPayload> scratchCardEvent = ScratchCardEventFactory.getScratchCardEvent(scratchCardEventPayload.getScratchCardEventType().name());
            if (scratchCardEvent != null) {
                ScratchCardPayload scratchCardPayload = new ScratchCardPayload();
                scratchCardPayload.setClient(scratchCardEventPayload.getClient());
                scratchCardPayload.setFlowName(scratchCardEventPayload.getFlowName());
                scratchCardPayload.setReferenceId(scratchCardEventPayload.getReferenceId());
                scratchCardPayload.setUserDetail(userDetail);
                scratchCardEvent.processEvent(scratchCardPayload);
            } else
                logger.info("no object found in factory for this event {} for ref id {} ", scratchCardEventPayload.getScratchCardEventType(), scratchCardEventPayload.getReferenceId());
        } catch (Exception exception) {
            logger.info("exception in consuming the scratch card event ref id {} ,flowname {} ,execption {}", scratchCardEventPayload.getReferenceId(), scratchCardEventPayload.getFlowName(), exception.getMessage());
        }
    }

    @KafkaListener(topics = "${spring.kafka.topic.post-scratch-card-event}", groupId = "${spring.kafka.consumer.group-id}")
    public void consumePostScratchCardEvent(ConsumerRecord<String, ScratchCardInfo> cr,
                                            @Payload ScratchCardInfo scratchCardInfo) {
        logger.info("scratch card id for post scratch card event {} payload of event {}", scratchCardInfo.getScratchCard().getId(), JSONUtil.getStringFromObject(scratchCardInfo));
        ScratchCard scratchCard = scratchCardInfo.getScratchCard();
        PostScratchProcess<ScratchCardInfo> postScratchProcess = OptionProcessFactory.getPostScratchProcess(scratchCard.getOfferType());
        if (postScratchProcess != null) {
            postScratchProcess.postProcessScratchCard(scratchCardInfo);
        }
    }


}
