package com.nobroker.scratchcard.entity;


import com.nobroker.scratchcard.enums.UserSource;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * Aayush Chaudhary
 */
@Entity
@Data
@Table(name = "user_details")
public class UserDetail {

    @Id
    @Column(name = "id")
    private String userId;
    @Column(name = "name")
    private String name;
    @Column(name = "mobile_number")
    private String mobileNumber;
    @Column(name = "email")
    private String email;
    @Column(name = "user_source")
    @Enumerated(EnumType.STRING)
    private UserSource userSource;
    @Column(name = "created_date")
    private Date createdDate;
}
