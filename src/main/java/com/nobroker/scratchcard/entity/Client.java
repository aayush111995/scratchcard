package com.nobroker.scratchcard.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Aayush Chaudhary
 */
@Entity
@Data
@Table(name = "client")
public class Client {
    @Id
    @Column(name = "id")
    private String id;
    @Column(name = "description")
    private String description;
}
