package com.nobroker.scratchcard.entity;

import com.nobroker.scratchcard.enums.CouponCompany;
import com.nobroker.scratchcard.enums.CouponType;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * Aayush Chaudhary
 */
@Entity
@Data
@Table(name = "coupon_codes")
public class CouponCode {

    @Id
    @Column(name = "id")
    private String id;
    @ManyToOne
    @JoinColumn(name = "coupon_source_id", referencedColumnName = "id", nullable = false)
    private CouponDetail couponDetail;
    @ManyToOne
    @JoinColumn(name = "client_id", referencedColumnName = "id", nullable = false)
    private Client client;
    @Column(name = "expiry_date")
    private Date expiryDate;
    @Column(name = "is_active")
    private Boolean isActive;
    @Column(name = "coupon_provider")
    @Enumerated(EnumType.STRING)
    private CouponCompany couponProvider;

    @Column(name = "coupon_type")
    @Enumerated(EnumType.STRING)
    private CouponType couponType;

    @Column(name = "created_date")
    private Date createdDate;
    @Column(name = "last_updated_date")
    private Date lastUpdatedDate;

}
