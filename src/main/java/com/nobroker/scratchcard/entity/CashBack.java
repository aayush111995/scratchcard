package com.nobroker.scratchcard.entity;

import com.nobroker.scratchcard.enums.CashBackState;
import com.nobroker.scratchcard.enums.CashTransactionType;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * Aayush Chaudhary
 */
@Entity
@Data
@Table(name = "cash_backs")
public class CashBack {

    @Id
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @GeneratedValue(generator = "uuid")
    private String id;

    @Column(name = "amount")
    private BigDecimal amount;
    @Column(name = "state")
    @Enumerated(EnumType.STRING)
    private CashBackState cashBackState;
    @Column(name = "transaction_id")
    private String transactionId;
    @Column(name = "transaction_type")
    @Enumerated(EnumType.STRING)
    private CashTransactionType cashTransactionType;
    @Column(name = "created_date")
    private Date createdDate;
    @Column(name = "last_updated_date")
    private Date lastUpdatedDate;
    @ManyToOne
    @JoinColumn(name = "contest_id", referencedColumnName = "id", nullable = false)
    private ContestDetail contestDetail;
}
