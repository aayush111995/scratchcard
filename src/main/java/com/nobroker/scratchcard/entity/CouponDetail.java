package com.nobroker.scratchcard.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Aayush Chaudhary
 */
@Entity
@Data
@Table(name = "coupon_details")
public class CouponDetail {

    @Id
    @Column(name = "id")
    private String id;
    @Column(name = "coupon_json")
    private String couponJson;
}
