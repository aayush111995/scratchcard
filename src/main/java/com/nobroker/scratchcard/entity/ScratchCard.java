package com.nobroker.scratchcard.entity;

import com.nobroker.scratchcard.enums.OfferType;
import com.nobroker.scratchcard.enums.ScratchCardState;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

/**
 * Aayush Chaudhary
 */
@Entity
@Data
@Table(name = "scratch_card", uniqueConstraints = {@UniqueConstraint(columnNames = {"offer_id", "offer_type"})})
public class ScratchCard {

    @Id
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @GeneratedValue(generator = "uuid")
    private String id;
    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    private UserDetail userDetail;

    @Column(name = "offer_id")
    private String offerId;

    @Column(name = "state")
    @Enumerated(EnumType.STRING)
    private ScratchCardState scratchCardState; // lock unlock viewed

    @Column(name = "offer_type")
    @Enumerated(EnumType.STRING)
    private OfferType offerType;

    @Column(name = "created_date")
    private Date createdDate;

    @Column(name = "last_updated_date")
    private Date lastUpdatedDate;

    @Column(name = "expiry_date")
    private Date expiryDate;

    @Column(name = "offer_reason")
    private String offerReason;

    @Column(name = "reference_id", unique = true)
    private String referenceId;

    @ManyToOne
    @JoinColumn(name = "contest_id", referencedColumnName = "id", nullable = false)
    private ContestDetail contestDetail;

}






