package com.nobroker.scratchcard.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Aayush Chaudhary
 */
@Entity
@Data
@Table(name = "contest_details")
public class ContestDetail {

    @Id
    @Column(name = "id")
    private String id;
    @Column(name = "created_date")
    private Date createdDate;
    @Column(name = "expiry_date")
    private Date expiryDate;
    @Column(name = "description")
    private String description;
    @JsonIgnore
    @Column(name = "options_json")
    private String optionsJson;
    @Column(name = "flow_name")
    private String flowName;
    @JsonIgnore
    @Column(name = "nb_cash_burn_value")
    private BigDecimal nbCashBurnValue;
    @JsonIgnore
    @Column(name = "min_cash_value")
    private BigDecimal minCashValue;
    @JsonIgnore
    @Column(name = "max_cash_value")
    private BigDecimal maxCashValue;
    @Column(name = "min_nb_cash_value")
    @JsonIgnore
    private BigDecimal minNbCashValue;
    @Column(name = "max_nb_cash_value")
    @JsonIgnore
    private BigDecimal maxNbCashValue;


}




