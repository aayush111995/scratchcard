package com.nobroker.scratchcard.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Aayush Chaudhary
 */
@Entity
@Data
@Table(name = "coupon_contest_map", uniqueConstraints = {@UniqueConstraint(columnNames = {"contest_id", "coupon_detail_id"})})
public class CouponContest {
    @Id
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @GeneratedValue(generator = "uuid")
    private String id;
    @Column(name = "contest_id")
    private String contestId;
    @Column(name = "coupon_detail_id")
    private String couponDetailId;
}
