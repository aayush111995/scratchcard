package com.nobroker.scratchcard.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Aayush Chaudhary
 */
@Service
public class RedisService {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Transactional(value = Transactional.TxType.NOT_SUPPORTED)
    public void deleteKey(String key) {
        final Boolean hasKey = redisTemplate.hasKey(key);
        if (Boolean.TRUE.equals(hasKey)) {
            redisTemplate.delete(key);
        }
    }

    public Optional<String> getValueFromCache(String key) {
        final ValueOperations<String, String> operations = redisTemplate.opsForValue();
        final Boolean hasKey = redisTemplate.hasKey(key);
        String returnVal = null;
        if (Boolean.TRUE.equals(hasKey)) {
            returnVal = operations.get(key);
        }
        return Optional.ofNullable(returnVal);
    }

    public void setKeyValueWithExpiry(String key, String value, Integer expiryInDays) {
        redisTemplate.opsForValue().set(key, value, expiryInDays, TimeUnit.DAYS);
    }

    public void setKeyValueWithExpiry(String key, String value, TimeUnit timeUnit, Integer expiry) {
        redisTemplate.opsForValue().set(key, value, expiry, timeUnit);
    }


    public void putKeyValuesInHash(String key, Map<String, String> keyValues, Integer expiryInMinutes) {
        redisTemplate.opsForHash().putAll(key, keyValues);
        redisTemplate.expire(key, expiryInMinutes, TimeUnit.MINUTES);
    }

    public String getValueFromHash(String key, String hashKey) {
        return (String) redisTemplate.opsForHash().get(key, hashKey);
    }


    public void setKeyValue(String key, String value) {
        redisTemplate.opsForValue().set(key, value);
    }

    public Boolean hasKey(String key) {
        if (!StringUtils.isEmpty(key)) {
            return redisTemplate.hasKey(key);
        } else {
            return Boolean.FALSE;
        }
    }

    public Long deleteKeyByPattern(String pattern) {
        if (!StringUtils.isEmpty(pattern)) {
            Set<String> keys = redisTemplate.keys(pattern);
            if (keys != null) {
                return redisTemplate.delete(keys);
            }
        }
        return -1L;
    }

}

