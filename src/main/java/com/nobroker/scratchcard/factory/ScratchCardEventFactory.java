package com.nobroker.scratchcard.factory;

import com.nobroker.scratchcard.config.MyApplicationContextProvider;
import com.nobroker.scratchcard.constants.ScratchCardConstants;
import com.nobroker.scratchcard.listener.ReleaseAndUnLockScratchCard;
import com.nobroker.scratchcard.listener.ReleaseScratchCard;
import com.nobroker.scratchcard.listener.ScratchCardEvent;
import com.nobroker.scratchcard.listener.UnLockScratchCard;
import com.nobroker.scratchcard.model.ScratchCardPayload;

/**
 * Aayush Chaudhary
 */
public class ScratchCardEventFactory {


    public static ScratchCardEvent<ScratchCardPayload> getScratchCardEvent(String event) {
        switch (event) {
            case ScratchCardConstants
                    .INITIATE:
                return MyApplicationContextProvider.getBean(ReleaseScratchCard.class);
            case ScratchCardConstants.INITIATE_AND_SUCCESS:
                return MyApplicationContextProvider.getBean(ReleaseAndUnLockScratchCard.class);
            case ScratchCardConstants.SUCCESS:
                return MyApplicationContextProvider.getBean(UnLockScratchCard.class);
            default:
                return null;
        }
    }
}
