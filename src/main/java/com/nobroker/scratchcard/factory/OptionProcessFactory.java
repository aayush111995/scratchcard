package com.nobroker.scratchcard.factory;


import com.nobroker.scratchcard.config.MyApplicationContextProvider;
import com.nobroker.scratchcard.enums.OfferType;
import com.nobroker.scratchcard.enums.Options;
import com.nobroker.scratchcard.model.ProcessOptionModel;
import com.nobroker.scratchcard.model.ScratchCardInfo;
import com.nobroker.scratchcard.service.*;

/**
 * Aayush Chaudhary
 */
public class OptionProcessFactory {

    public static OptionProcess<ProcessOptionModel> getOptionProcessObj(Options option) {
        switch (option) {
            case COUPON:
                return MyApplicationContextProvider.getBean(CouponOptionProcess.class);
            case CASHBACK:
                return MyApplicationContextProvider.getBean(CashBackOptionProcess.class);
            case NBCASH:
                return MyApplicationContextProvider.getBean(NbCashOptionProcess.class);
            case BETTERLUCK:
                return MyApplicationContextProvider.getBean(BetterLuckOptionProcess.class);
            default:
                return null;
        }
    }

    public static PostScratchProcess<ScratchCardInfo> getPostScratchProcess(OfferType offerType) {
        switch (offerType) {
            case NB_CASH:
                return MyApplicationContextProvider.getBean(NbCashOptionProcess.class);
            case CASHBACK:
                return MyApplicationContextProvider.getBean(CashBackOptionProcess.class);
            case BETTER_LUCK:
                return MyApplicationContextProvider.getBean(BetterLuckOptionProcess.class);
            default:
                return null;
        }
    }
}
