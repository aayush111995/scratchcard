package com.nobroker.scratchcard.factory;

import com.nobroker.scratchcard.enums.ScratchCardRandomStrategyName;
import com.nobroker.scratchcard.service.ScratchCardRandomStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Aayush Chaudhary
 */

@Service
public class ScratchCardPickStrategyFactory {
    private Map<ScratchCardRandomStrategyName, ScratchCardRandomStrategy> strategies;

    @Autowired
    public ScratchCardPickStrategyFactory(Set<ScratchCardRandomStrategy> strategySet) {
        createStrategy(strategySet);
    }

    public ScratchCardRandomStrategy findStrategy(ScratchCardRandomStrategyName strategyName) {
        return strategies.get(strategyName);
    }

    private void createStrategy(Set<ScratchCardRandomStrategy> strategySet) {
        strategies = new HashMap<>();
        strategySet.forEach(
                strategy -> strategies.put(strategy.getScratchCardRandomStrategyName(), strategy));
    }


}
