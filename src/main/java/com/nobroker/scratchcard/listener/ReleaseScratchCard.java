package com.nobroker.scratchcard.listener;

import com.nobroker.scratchcard.entity.ContestDetail;
import com.nobroker.scratchcard.enums.Options;
import com.nobroker.scratchcard.enums.ScratchCardState;
import com.nobroker.scratchcard.factory.OptionProcessFactory;
import com.nobroker.scratchcard.model.OptionJson;
import com.nobroker.scratchcard.model.ProcessOptionModel;
import com.nobroker.scratchcard.model.ScratchCardPayload;
import com.nobroker.scratchcard.service.ContestDetailService;
import com.nobroker.scratchcard.service.OptionProcess;
import com.nobroker.scratchcard.utils.JSONUtil;
import com.nobroker.scratchcard.utils.ScratchCardUtils;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Aayush Chaudhary
 */
@Service
@AllArgsConstructor
public class ReleaseScratchCard implements ScratchCardEvent<ScratchCardPayload> {
    public static Logger logger = LoggerFactory.getLogger(ReleaseScratchCard.class);

    private final ContestDetailService contestDetailService;

    @Override
    public void processEvent(ScratchCardPayload scratchCardPayload) {
        logger.info("release scratch card processing starts ");
        Map<String, OptionJson> fallBackWithOptions = null;
        List<ContestDetail> contestDetailList = contestDetailService.getContestsByFlowName(scratchCardPayload.getFlowName());
        if (contestDetailList != null && !contestDetailList.isEmpty()) {
            int randomValue = ScratchCardUtils.getRandomValueInRange(1, contestDetailList.size());
            ContestDetail contestDetail = contestDetailList.get(randomValue - 1);
            OptionJson optionJson = JSONUtil.getObjectFromJsonString(contestDetail.getOptionsJson(), OptionJson.class);
            Options option = getScratchCard(optionJson);
            if (optionJson != null) {
                fallBackWithOptions = mapFallBackWithOptions(optionJson);
            }
            OptionProcess<ProcessOptionModel> optionProcess = OptionProcessFactory.getOptionProcessObj(option);
            ProcessOptionModel processOptionModel = new ProcessOptionModel();
            processOptionModel.setScratchCardPayload(scratchCardPayload);
            processOptionModel.setContestDetail(contestDetail);
            processOptionModel.setOptionJson(optionJson);
            processOptionModel.setScratchCardState(ScratchCardState.LOCKED);
            if (optionProcess != null) {
                boolean isProcessed = optionProcess.processOption(processOptionModel);
                logger.info("option process  release status {} reference id {} ", isProcessed, scratchCardPayload.getReferenceId());

                if (!isProcessed && fallBackWithOptions != null) {
                    OptionJson fallBackJson = fallBackWithOptions.get(option.name());

                    if (fallBackJson != null) {
                        boolean isProcessFallBack = processFallBack(fallBackJson, processOptionModel);
                        logger.info("processfallback release  status {} reference id {} ", isProcessFallBack, scratchCardPayload.getReferenceId());
                    } else {
                        logger.info("fallbackjson is null  release and unlock reference id {} ", scratchCardPayload.getReferenceId());
                    }

                }
            }

        }
    }


}
