package com.nobroker.scratchcard.listener;

import com.nobroker.scratchcard.entity.ScratchCard;
import com.nobroker.scratchcard.enums.ScratchCardState;
import com.nobroker.scratchcard.model.ScratchCardPayload;
import com.nobroker.scratchcard.service.ScratchCardService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

/**
 * Aayush Chaudhary
 */
@Service
@AllArgsConstructor
public class UnLockScratchCard implements ScratchCardEvent<ScratchCardPayload> {

    public static Logger logger = LoggerFactory.getLogger(UnLockScratchCard.class);

    private final ScratchCardService scratchCardService;

    @Override
    public void processEvent(ScratchCardPayload scratchCardPayload) {
        Optional<ScratchCard> scratchCardOptional = scratchCardService.findScratchCardByRefId(scratchCardPayload.getReferenceId());
        if (scratchCardOptional.isPresent()) {
            ScratchCard scratchCard = scratchCardOptional.get();
            scratchCard.setScratchCardState(ScratchCardState.UNLOCKED);
            scratchCard.setLastUpdatedDate(new Date());
            scratchCardService.save(scratchCard);
        }
    }

}
