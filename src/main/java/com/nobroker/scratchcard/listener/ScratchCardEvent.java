package com.nobroker.scratchcard.listener;

import com.nobroker.scratchcard.config.MyApplicationContextProvider;
import com.nobroker.scratchcard.constants.ScratchCardConstants;
import com.nobroker.scratchcard.enums.Options;
import com.nobroker.scratchcard.enums.ScratchCardRandomStrategyName;
import com.nobroker.scratchcard.factory.OptionProcessFactory;
import com.nobroker.scratchcard.factory.ScratchCardPickStrategyFactory;
import com.nobroker.scratchcard.model.OptionJson;
import com.nobroker.scratchcard.model.ProcessOptionModel;
import com.nobroker.scratchcard.service.OptionProcess;
import com.nobroker.scratchcard.service.ScratchCardRandomStrategy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * Aayush Chaudhary
 */
public interface ScratchCardEvent<R> {

    void processEvent(R r);

    default Options getScratchCard(OptionJson optionJson) {
        ScratchCardRandomStrategy strategy = MyApplicationContextProvider.getBean(ScratchCardPickStrategyFactory.class).
                findStrategy(ScratchCardRandomStrategyName.MODULUS_PICK);
        return strategy.pickRandomOption(optionJson);

    }

    default Map<String, OptionJson> mapFallBackWithOptions(OptionJson optionJson) {
        Map<String, OptionJson> mapFallBack = new HashMap<>();
        mapFallBack.put(ScratchCardConstants.COUPON, optionJson.getCouponFallBack());
        mapFallBack.put(ScratchCardConstants.NBCASH, optionJson.getNbCashFallBack());
        mapFallBack.put(ScratchCardConstants.CASHBACK, optionJson.getCashBackFallBack());
        return mapFallBack;
    }


    default boolean processFallBack(OptionJson fallBackJson, ProcessOptionModel processOptionModel) {

        Map<Options, Integer> map = new TreeMap<>();
        if (fallBackJson.getCoupon() != null)
            map.put(Options.COUPON, Integer.parseInt(fallBackJson.getCoupon()));
        if (fallBackJson.getCashBack() != null)
            map.put(Options.CASHBACK, Integer.parseInt(fallBackJson.getCashBack()));
        if (fallBackJson.getNbCash() != null)
            map.put(Options.NBCASH, Integer.parseInt(fallBackJson.getNbCash()));
        if (fallBackJson.getBetterLuckNextTime() != null)
            map.put(Options.BETTERLUCK, Integer.parseInt(fallBackJson.getBetterLuckNextTime()));
        List<Map.Entry<Options, Integer>> list = map.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue()).collect(Collectors.toList());

        for (Map.Entry<Options, Integer> entry : list) {
            OptionProcess<ProcessOptionModel> optionProcessFallBack = OptionProcessFactory.getOptionProcessObj(entry.getKey());
            if (optionProcessFallBack != null) {
                boolean isProcessedFallBack = optionProcessFallBack.processOption(processOptionModel);
                if (isProcessedFallBack) return isProcessedFallBack;
            }
        }
        return false;
    }
}
