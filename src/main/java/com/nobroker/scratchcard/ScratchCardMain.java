package com.nobroker.scratchcard;

import com.nobroker.scratchcard.config.MyApplicationContextProvider;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * Aayush Chaudhary
 */
@SpringBootApplication
public class ScratchCardMain {


    public static void main(String[] args) {


        ConfigurableApplicationContext configurableApplicationContext = SpringApplication.run(ScratchCardMain.class, args);
        MyApplicationContextProvider.setApplicationContext(configurableApplicationContext);

    }


}
